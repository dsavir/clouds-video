import java.util.LinkedList;
/**
 class representing a lot of moving points, all moving according to perlin noise
 
 Copyright 2020 by Danielle Honigstein. Free to use any way you like with attribution please.
 
 */
class FrostBurst {
  LinkedList<MovingPoint> points = new LinkedList();//our points
  LinkedList<Integer> toErase = new LinkedList();//index of points to erase if out of the window frame
  color c;//color of the burst
  int numPoints;//number of points in burst
  int seed;//seed for noise, you can experiment with this. if you choose the same seed for all bursts they will be the same.

  /**
   create a frostburst starting at point 'x','y', with 'numpoint' points, all of color 'c'
   */
  FrostBurst(float x, float y, int numPoints, color c) {
    this.c = c;
    this.numPoints = numPoints;
    //create the MovingPoints
    for (int i = 0; i<numPoints; i++) {
      points.push(new MovingPoint(x, y));
    }
    //initialize seed
    seed = ceil(random(100000));
    noiseSeed(seed);
  }
  /**
   move all the points one step and draw them
   */
  void step() {
    for (int i = (numPoints-1); i>0; i--) {
      points.get(i).step();
      stroke(c);
      points.get(i).display();
      //remove points that are out of frame.
      float x = points.get(i).x;
      float y = points.get(i).y;
      if (x>width || x<0 || y>height || y<0) {
        points.remove(i);
        numPoints--;
      }
    }
    //println(numPoints);//can be used for debug
  }
}
