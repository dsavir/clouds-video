/**
 Code for creating "frost" bursts on beat onset in music. As always with video export, it's
 best to do two passes: one to calculate the music analysis and save in text file, and another that reads from
 text file, draws and saves. otherwise the audio and video go out of sync.
 
 Inspired by https://imgur.com/a/yXq1yuu#8hso4xr, though probably the code is not in the least similar ;)
 
 Try changing the number of points, the transparency, the deltas in MovingPoint and the scale in MovingPoint, you'll
 get beautiful and completely different results.
 
 Copyright 2020 by Danielle Honigstein. Free to use any way you like with attribution please.
 */
//video export library
import com.hamoid.*;
//minim library for music analysis
import ddf.minim.*;
import ddf.minim.analysis.*;
//minim stuff
Minim minim;
AudioPlayer song;
BeatDetect beat;
BeatListener bl;
//video export stuff
VideoExport videoExport;
//io
PrintWriter output;
BufferedReader reader;
//video variables
int numPoints = 1500;//number of points per burst
LinkedList<FrostBurst> frost = new LinkedList();//all our frostbursts
float alphaValue = 30;//the starting transparency of each point
int numBursts = 1;//number of starting frostbursts
color[] colors = {color(51, 255, 255, alphaValue), color(255, 255, 255, alphaValue), color(255, 255, 51, alphaValue), color(255, 51, 255, alphaValue)};//colors of the frostbursts
int colorInd = 0;//index in color array
//timing variables
long lastTime = 0;
long lastFade = 0;
//will be written to text file - to create a burst in this frame?
int toCreate = 0;


int counter = 0;//for debug, track how many bursts created
//pass mode.
int mode = 2;//0 - view. 1 - save file. 2 - record video.

//based on the minim examples: beatlistener to detect beat onset.
class BeatListener implements AudioListener
{
  private BeatDetect beat;
  private AudioPlayer source;

  BeatListener(BeatDetect beat, AudioPlayer source)
  {
    this.source = source;
    this.source.addListener(this);
    this.beat = beat;
  }

  void samples(float[] samps)
  {
    beat.detect(source.mix);
  }

  void samples(float[] sampsL, float[] sampsR)
  {
    beat.detect(source.mix);
  }
}


void setup() {
  size(400, 400, P3D);
  background(0);

  createBursts(colors[colorInd]);//first burst

  if (mode==2) {//create reader for text file and initialize video export
    reader = createReader(dataPath("Test.txt"));
    videoExport = new VideoExport(this);
    videoExport.setAudioFileName("clouds-clouds-snow.mp3");
    videoExport.startMovie();
  } else if (mode ==1) { //create writer for text file
    output = createWriter(dataPath("Test.txt"));
  }
  minim = new Minim(this);
  song = minim.loadFile("clouds-clouds-snow.mp3", 1024);
  song.play();
  // a beat detection object song SOUND_ENERGY mode with a sensitivity of 1000 milliseconds. too sensitive gives too many bursts
  if (mode==0 | mode ==1) {
    beat = new BeatDetect(song.bufferSize(), song.sampleRate());
    beat.setSensitivity(1000);
    bl = new BeatListener(beat, song);
  }
  lastTime = millis();//initialize start time. we want to spread the bursts out, so we will define the minimium time between them
  lastFade = lastTime;//initialize fading mechanism
}


void draw() {
  String line = "";
  int p;
  //move all bursts
  for (int i = 0; i<frost.size(); i++) {
    frost.get(i).step();
  }
  if (mode==2) { //if recrding, try to read from file
    try {
      line = reader.readLine();
    }
    catch (IOException e) {//exit if error
      e.printStackTrace();
      line = null;
    }
    if (line == null) {//end of file, end movie and exit
      videoExport.endMovie();
      exit();
    }
  }
  if (mode!=2) {//not recording, calculate beat and check if at least 500 ms since lsat burst
    if (beat.isKick() && millis()-lastTime>500) {
      lastTime = millis();
      if (mode==1) {//if wirting, update variable
        toCreate = 1;
      } else {//if drawing, create new burst
        createBursts(colors[colorInd]);
      }
    } else {
      toCreate = 0;
    }
  } else {//recording, read from file
    p = parseInt(line);
    if (p==1) {//if tocreate was 1, create burst
      createBursts(colors[colorInd]);
    }
    videoExport.saveFrame();//in any case save frame
  }

  if (mode==1) {//if writing to text file, write variable
    output.println(toCreate);
  }
  //fading mechanism - older points are slowly faded by drawing on top of everything a black semi transparent rectangle every 100 ms
  if (millis()-lastFade>100) {
    fill(0, 0, 0, 10);
    noStroke();
    rect(0, 0, width, height);
    lastFade = millis();
  }
}
//this was for debug, not used in video
void mousePressed() {
  createBursts(colors[colorInd]);
}
//create a new frostburst with color c, at a random location
void createBursts(color c) {
  for (int i = 0; i<numBursts; i++) {
    frost.push(new FrostBurst(random(0.25*width, 0.9*width), random(0.25*height, 0.9*height), numPoints, c));
  }
  colorInd++;//update color index
  if (colorInd>=colors.length) {
    colorInd=0;
  }
  counter++;//write how many bursts created, for debug
  println("created "+ counter);
}

//it's supposed to exit by itself when movie done, but if no movie created (mode 1 or 0) can press q to exit.
void keyPressed() {
  if (key == 'q') {
    if (mode==2) {//to be on the safe side
      videoExport.endMovie();
    } else if (mode==1) {//close file properly
      output.flush();
      output.close();
    }
    exit();//and in any case exit
  }
}
