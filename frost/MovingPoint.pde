/**
 a point that moves in x and y with perlin noise(https://processing.org/reference/noise_.html).
 perlin noise is a bit tricky to use...
 try changing the ranges and the divsor of movement (currently 80) and see what differnet results you get
 
 Copyright 2020 by Danielle Honigstein. Free to use any way you like with attribution please.
 
 */
class MovingPoint {
  float x, y;//current position of point
  float range = 1;//range for x and y offset of noise, 
  float rangeX = 0.5;//range for delta x off noise
  float rangeY = rangeX;//range for delta y of noise
  float xoff = random(range)-range/2;//x offset for noise
  float yoff=random(range)-range/2; //y offset for noise
  float deltaX, deltaY;//delta x and y for noise

  /**
   create a new moving point at location 'x','y'
   */
  MovingPoint(float x, float y) {
    this.x = x;
    this.y = y;
    //initialize deltaX and y with random values in given range
    deltaX = random(rangeX) - rangeX/2;
    deltaY = random(rangeY) - rangeY/2;
  }

  /**
   draw point
   */
  void display() {
    point(x, y);
  }

  /**
   move the point according to perlin noise
   */
  void step() {
    xoff = xoff + deltaX;
    yoff = yoff + deltaY;
    float n = (noise(xoff)*width-width/2)/80;
    float m = (noise(yoff)*height-height/2)/80;
    x = x+n;
    y = y+m;
  }
}
