# Clouds Video - Danielle Honigstein

Code for my video Clouds (https://youtu.be/0BVqFYParRs). 
First part (and last) is clouds-full-video using BurstingSpheres: a number of colored spheres that burst out of a random point at a given speed and gravity.
Second verse: frost using FrostBursts: 1500 points moving with perlin noise, creates lovely patterns
The rest of the video was ink diffusion or pixabay (https://pixabay.com)

Code is free to use for whatever you wish with attribution (See license).

Copyright 2020 Danielle Honigstein
