/** //<>//
 Creates a full length video of bursting spheres on notes of clouds.mp3.
 As can be seen in the Video export examples, exporting a video of sound visualization in one pass makes the 
 audio and video out of sync. So first pass we calculate when the bursting spheres should appear and save in text file,
 and in second pass we draw the spheres and export video.
 In order to detect the notes I looked at the maximum values of the fft band. Beat onset was not reliable in this case as it isn't really
 a beat, it's the notes htemselves I was looking for.
 
 Copyright 2020 by Danielle Honigstein. Free to use any way you like with attribution please.

 */
import java.util.LinkedList;
//minim. get from tools->add tools->libraries->minim
import ddf.minim.*;
import ddf.minim.analysis.*;
import com.hamoid.*;

//variables for beat detection
Minim minim;
AudioPlayer song;
FFT fftLin;
BeatDetect beat;
//paths
String audioFilePath = "clouds.mp3";
String textFilePath = audioFilePath + ".txt";

//timing
long prev = 0;
long curr = 0;

//all our spheres
LinkedList<BurstingSpheres> spheres = new LinkedList();

//video export, to make a video from processing code
VideoExport videoExport;
float movieFPS = 60;
float frameDuration = 1 / movieFPS;

//file i/o
BufferedReader reader;
PrintWriter output;

//first pass: create text file. Second pass: get beat data from text file and create video
boolean firstPass = false;

//keep track of maximum fft value
float max;

//counter to keep the spheres at least 350 ms apart
int counter = 0;


//setup variables
void setup() {
  frameRate(movieFPS);
  size(1440, 810, P3D);
  background(200, 30, 90);


  if (firstPass) {//calculate the onset and create text file. don't draw and don't export.
    output = createWriter(dataPath(textFilePath));//filewriter
    minim = new Minim(this);
    song = minim.loadFile(audioFilePath, 512);
    song.play();
    beat = new BeatDetect();

    fftLin = new FFT( song.bufferSize(), song.sampleRate() );
    fftLin.linAverages( 30 );
  } else {//read data and setup vidoexport
    reader = createReader(textFilePath);
    // Set up the video exporting
    videoExport = new VideoExport(this);
    videoExport.setFrameRate(movieFPS);
    videoExport.setAudioFileName(audioFilePath);
    videoExport.startMovie();
    //setup cameras and lights for 3D drawing
    ortho();
    noStroke();
    fill(127, 127, 250, 200);
    lights();
    colorMode(HSB, 360, 100, 100);
  }
}


void draw() {
  if (firstPass) { //calculate and write to text file
    fftLin.forward( song.mix );
    float max = 0;
    curr = millis();
    if (curr-prev>50) { //check every 50 ms
      for (int i = 0; i < fftLin.specSize(); i++)
      {
        if (fftLin.getBand(i)>max) {
          max = fftLin.getBand(i);
        }
      }
      counter++;//update counter every 50 ms

      output.println(max + ","  + counter);
      if (max>3 && counter > 7) {//if fft spectrum is high and at least 350 ms passed since last time, reset counter.
        counter = 0;
      }
    }
    if (!(song.isPlaying())) {//song has finished - close and exit.
      println("finished song");//debug
      output.flush();
      output.close();
      exit();
    }
  } else { //draw and export
    String line;
    curr = millis();
    if (curr-prev>50) {//read every 50 ms
      try {
        line = reader.readLine();
        //count++;
      }
      catch (IOException e) {
        e.printStackTrace();
        line = null;
      }
      if (line == null) {
        // Done reading the file.
        // Close the video file.
        println("finished file");
        videoExport.endMovie();
        exit();
      } else { //parse line and draw accordingly
        background(200, 30, 90);
        lights();
        directionalLight(255, 255, 255, -1, 0, 0);
        for (int i = 0; i< spheres.size(); i++) { //move existing spheres
          BurstingSpheres tmp = spheres.get(i);
          tmp.move();
          if (tmp.size()==0) {//remove empty spheres
            spheres.remove(tmp);
          }
        }
        //save frame
        videoExport.saveFrame();
        //create new spheres if needed
        String[] p = split(line, ",");
        max = float(p[0]);
        counter = int(p[1]);
        if (max>3 && counter >7) {
          PVector center = new PVector(random(10, width-10), random(10, height-10));
          spheres.push(new BurstingSpheres((int)random(5, 10), color(random(0, 360), random(20, 85), random(85, 95)), center, 5, 5, 0.5));
        }
        prev = curr;
      }
    }
  }
}
