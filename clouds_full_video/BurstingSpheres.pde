/**
 class representing spheres bursting from a given point. each sphere is a MovingSphere.
 */
class BurstingSpheres {
  int numSpheres;//number of spheres in group
  LinkedList<MovingSphere> spheres;//all our spheres
  /**
   create new BurstingSpheres, with 'numSphere's spheres, starting at point 'start', moving at speed 'speed'
   fading at rate 'alphaStep', growing by 'radiusStep' each movement.
   */
  BurstingSpheres(int numSpheres, color c, PVector start, float speed, int alphaStep, float radiusStep) {
    this.numSpheres = numSpheres;
    spheres = new LinkedList();
    MovingSphere tmp;
    for (int i = 0; i<numSpheres; i++) {
      PVector dir = PVector.random2D().mult(speed);//random direction, given speed
      tmp = new MovingSphere(new PVector(start.x, start.y), dir, c);
      tmp.setRadiusStep(radiusStep);
      tmp.setAlphaStep(alphaStep);
      spheres.push(tmp);
    }
  }

  /**
   move all the spheres
   */
  void move() {
    for (int i = 0; i< numSpheres; i++) {
      MovingSphere tmp = spheres.get(i);
      //println(tmp.toString());
      tmp.draw();
      tmp.move();
      if (!tmp.isVisible()) {//remove faded spheres
        spheres.clear();
        return;
      }
    }
  }

  /**
   return the number of spheres in this BurstingSphere.
   */
  int size() {
    return spheres.size();
  }
}
