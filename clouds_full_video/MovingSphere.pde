/**
 Class representing a single moving sphere. the sphere moves from a given center in a given direction, and also has gravity pulling it down.
 The sphere fades with time, and changes radius with time as well.
 */
class MovingSphere {
  //center of sphere
  PVector center;
  //direction of movement
  PVector dir;
  //color of sphere
  color c;
  //radius of sphere
  float radius;
  //radius change over time
  float radiusStep = 1;
  //transparency
  int alpha;
  //tracnsparency change over time
  int alphaStep = 10;
  //gracity pulling the sphere down
  PVector gravity = new PVector(0, 0.3);

  /**
   Create a moving sphere at center 'center', moving in direction 'dir', of color 'c'
   */
  MovingSphere(PVector center, PVector dir, color c) {
    this.center = center; 
    this.dir = dir;
    this.c = c;
    radius = 10;//defualt radius
    alpha = 255;//starts completely opaque
  }

  /**
   draw the sphere.
   */
  void draw() {
    lights();
    fill(hue(c), saturation(c), brightness(c), alpha);
    translate(center.x, center.y);
    sphere(radius);
    translate(-center.x, -center.y);
  }

  /**
   change the rate of fading
   */
  void setAlphaStep(int alphaStep) {
    this.alphaStep = alphaStep;
  }

  /**
   change the radius growth rate
   */
  void setRadiusStep(float radiusStep) {
    this.radiusStep = radiusStep;
  }

  /**
   move the sphere in direction dir and gravity.
   update new radius size and alpha.
   */
  void move() {
    center.add(dir);
    dir.add(gravity);
    radius = radius + radiusStep;
    alpha = alpha -alphaStep;
  }

  /**
   String representation of sphere for debug.
   */
  String toString() {
    return "center: " + center + "dir: " + dir;
  }

  /**
   check if sphere is visible (alpha >0).
   */
  boolean isVisible() {
    return alpha>0;
  }
}
